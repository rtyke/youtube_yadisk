from pytube import YouTube, exceptions
import requests
from urllib.parse import urlencode
import os
import time
import sys

import config as cfg

AUTH_HEADER = {'Authorization': 'OAuth ' + cfg.YDISK_TOKEN}
API_HREF = 'https://cloud-api.yandex.net/v1/disk/'
YDISK_STORAGE = {'path': cfg.YDISK_DIR}
PROJECT_PATH = os.path.dirname(os.path.realpath(__file__))

# TODO the best way to store token?
# TODO failed on url https://www.youtube.com/watch?v=j3XufmvEMiM&list=PLWKjhJtqVAbn5emQ3RRG8gEBqkhf_5vxDpy


def get_streams(url):
    streams = YouTube(url,
                      on_progress_callback=generate_progress_bar).streams.all()
    return streams


def output_streams(streams):
    for c, stream in enumerate(streams):
        print(f'{(c + 1)}. {stream}')


def choose_stream(streams):
    while True:
        stream_counter_input = input('Please enter number of stream\n')
        try:
            stream_counter = int(stream_counter_input)
            if stream_counter < 1 or stream_counter > len(streams):
                print(f'<{stream_counter}> - is wrong number. '
                      f'Please, choose number from above list')
                continue
            return stream_counter
        except ValueError:
            print(f'<{stream_counter_input}> - is not number. '
                  f'Please, choose number from above list')


def create_arg_parser():
    # TODO use this
    # TODO delete video from hardrive
    # TODO choose directory on harddrive to save video
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', 'youtube url to download')


def choose_video(youtube_link):
    vid_streams = get_streams(youtube_link)
    output_streams(vid_streams)
    stream_choice = choose_stream(vid_streams) - 1
    video = vid_streams[stream_choice]
    return video


def generate_progress_bar(stream, chunk, file_handle, bytes_remaining):
    # callback function
    downloaded_percent = int((VIDEO_SIZE - bytes_remaining) / VIDEO_SIZE * 100)
    draw_progress_bar(percents_done=downloaded_percent)


def draw_progress_bar(percents_done=100, one_bar=10):
    if not percents_done % one_bar:
        done = 'X' * (percents_done // one_bar)
        remained = ' ' * (one_bar - percents_done // one_bar)
        at_time = time.strftime('%H:%M:%S')
        progress_bar = f'|\r{done}{remained}| {percents_done}% [{at_time}]'
        print(progress_bar, end='')


def youtube_url_is_valid(url):
    try:
        YouTube(url)
        return True
    except exceptions.RegexMatchError:
        return False


def token_is_valid():
    req = requests.get(API_HREF, headers=AUTH_HEADER)
    if req.status_code == 200:
        return True
    else:
        return False


def set_ydisk_dir():
    req = requests.get(f'{API_HREF}resources', params=YDISK_STORAGE, headers=AUTH_HEADER)
    if req.status_code == 404:
        requests.put(f'{API_HREF}resources', params=YDISK_STORAGE, headers=AUTH_HEADER)


def get_url():
    try:
        youtube_url = sys.argv[1]
    except IndexError:
        youtube_url = input('Please input youtube link for download :\n')
    return youtube_url


def video_is_uploaded(filename):
    req = requests.get(f'{API_HREF}resources', params=YDISK_STORAGE, headers=AUTH_HEADER)
    files_on_ydisk = req.json()['_embedded']['items']
    filenames_on_ydisk = list(map(lambda x: x['name'], files_on_ydisk))
    return filename in filenames_on_ydisk


def calculate_ydisk_freespace():
    req = requests.get(API_HREF, headers=AUTH_HEADER)
    disk_properties = req.json()
    free_space = disk_properties['total_space'] - disk_properties['used_space']
    return free_space


def upload_video_on_ydisk(video_name):
    with open(PROJECT_PATH + '/vid/' + video_name, 'rb') as f:
        binary_data = f.read()
    upload_url = get_upload_url(video_name)
    # TODO add progress bar
    # https: // stackoverflow.com / questions / 13909900 / progress - of - python - requests - post
    upload = requests.put(upload_url, data=binary_data)
    # TODO handle status codes other than 201
    return upload.status_code


def get_upload_url(video_name):
    ydisk_video_path = urlencode({'path': cfg.YDISK_DIR + '/' + video_name})
    upload_href = f'{API_HREF}resources/upload?{ydisk_video_path}'
    req = requests.get(upload_href, headers=AUTH_HEADER)
    return req.json()['href']


if __name__ == '__main__':
    youtube_url = get_url()
    # TODO handle connection glitch urllib.error.URLError: <urlopen error [Errno 54] Connection reset by peer
    if not youtube_url_is_valid(youtube_url):
        sys.exit(f'{youtube_url} is not correct youtube url')
    elif not token_is_valid():
        sys.exit('Token for ydisk is not valid, please check it')

    set_ydisk_dir()
    ydisk_free_space = calculate_ydisk_freespace()

    video_to_download = choose_video(youtube_url)
    video_name = video_to_download.default_filename
    VIDEO_SIZE = video_to_download.filesize

    if video_is_uploaded(video_name):
        sys.exit(f'{video_name} already uploaded on yandex disk')
    elif VIDEO_SIZE > ydisk_free_space:
        sys.exit(f'Not enough space {ydisk_free_space} for file {VIDEO_SIZE} on ydisk')

    video_to_download.download(PROJECT_PATH + '/vid/')
    upload_video_on_ydisk(video_name)

    # clean
    os.remove(f'{PROJECT_PATH}/vid/{video_name}')








